﻿using UnityEngine;
using System.Collections;

public class M9Damage : MonoBehaviour {
	int DamageAmount = 5;
	public float TargetDistance;
	public float MaxRange = 15;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {
			RaycastHit shot;
			if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out shot)){
				TargetDistance = shot.distance;
				if(TargetDistance < MaxRange){
					shot.transform.SendMessage("RemoveHealth", DamageAmount, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}
}
